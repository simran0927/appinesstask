package com.simran.app

import android.app.Application
import com.simran.di.viewModels
import com.simran.util.extension.SharedPreferenceUtil
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin


class App : Application() {
    val preference by lazy { SharedPreferenceUtil(this) }
    override fun onCreate() {
        super.onCreate()
        application = this
        startKoin {
            androidContext(this@App)
            modules(listOf(viewModels))
        }
    }

    companion object {
        lateinit var application: App
        @JvmStatic
        fun getApp() = application
    }

}

fun Any.getPref(): SharedPreferenceUtil {
    return App.getApp().preference
}