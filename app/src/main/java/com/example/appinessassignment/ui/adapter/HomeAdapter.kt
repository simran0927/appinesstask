package com.example.appinessassignment.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.appinessassignment.R
import com.example.appinessassignment.model.ViewData
import kotlinx.android.synthetic.main.item_data.view.*

class HomeAdapter(val context: Context): RecyclerView.Adapter<HomeAdapter.ViewHolder>() {
    protected val list: ArrayList<ViewData> = arrayListOf()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view=LayoutInflater.from(parent.context).inflate(R.layout.item_data,parent,false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
holder.itemView.title_text.text=context.resources.getString(R.string.title_s,list[position].title)
        holder.itemView.backers_text.text=context.resources.getString(R.string.number_of_backers_s,list[position].num_backers)
        holder.itemView.by_text.text=context.resources.getString(R.string.by_s,list[position].by)
    }

    class ViewHolder(itemView:View):RecyclerView.ViewHolder(itemView) {

    }

    fun addNewList(list: ArrayList<ViewData>?) {
        if (list != null) {
            this@HomeAdapter.list.clear()
            this@HomeAdapter.list.addAll(list)
            notifyDataSetChanged()
        }
    }
}