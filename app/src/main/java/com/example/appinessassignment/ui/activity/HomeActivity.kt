package com.example.appinessassignment.ui.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.Toast
import androidx.lifecycle.Observer
import com.example.appinessassignment.R
import com.example.appinessassignment.model.ViewData
import com.example.appinessassignment.ui.adapter.HomeAdapter
import com.example.appinessassignment.viewModel.HomeViewModel
import com.simran.util.extension.isNotNull
import kotlinx.android.synthetic.main.activity_home.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*
import kotlin.collections.ArrayList





class HomeActivity : AppCompatActivity() {

    val homeViewModel by viewModel<HomeViewModel>()
    val homeAdapter:HomeAdapter by lazy { HomeAdapter(this) }
    var originalList:ArrayList<ViewData>?= arrayListOf()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        home_data_rv.adapter = homeAdapter
        observeData()
        homeViewModel.getHomeData("http://enterprisesmail.com/json/api.json")
        searchData()
    }


    private fun observeData(){
        homeViewModel.homeDataObserver.observe(this, Observer {
            if(it.isNotNull()) {
               originalList =it
                homeAdapter.addNewList(sortList())

            }else{
             Toast.makeText(this,"Data not found",Toast.LENGTH_LONG).show()
            }
        })
    }


    private fun searchData(){
        search_data.addTextChangedListener(object :TextWatcher{
            override fun afterTextChanged(s: Editable?) {
                filter(s.toString())
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })
    }

    fun filter(text: String) {
        val temp : ArrayList<ViewData>?= arrayListOf()
        sortList()?.let {
            it.forEach {
            if(it.title.contains(text,true)){
                temp?.add(it)
            }

            }
        }

        homeAdapter?.addNewList(temp)
    }


    private fun sortList():ArrayList<ViewData>{
        val compareByFirstName =
            { o1: ViewData, o2: ViewData -> o1.title.compareTo(o2.title) }
         Collections.sort(originalList,compareByFirstName)
    return originalList!!
    }

}
