package com.example.appinessassignment.ui.activity

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.content.ContextCompat
import com.example.appinessassignment.R
import com.simran.util.extension.delay
import com.simran.util.extension.isNetworkAvailable
import com.simran.util.extension.openActivity

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    initViews()
    }

    private fun initViews(){
        if(isNetworkAvailable()){
        delay(3000) {
            finish()
            openActivity<HomeActivity>()
        }}else{
            val alertDialog = AlertDialog.Builder(this).create()
            alertDialog.setTitle("Error")
            alertDialog.setMessage("Check your internet connection and try again.")
            alertDialog.setButton(
                DialogInterface.BUTTON_POSITIVE, "Try Again",
                DialogInterface.OnClickListener { dialog, which ->
                    finish()
                    startActivity(intent)
                })

            alertDialog.show()

        }
    }
}
