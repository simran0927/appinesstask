package com.example.appinessassignment.viewModel

import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import apiHitter
import com.example.appinessassignment.base.BaseViewModel
import com.example.appinessassignment.model.ViewData
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class HomeViewModel : BaseViewModel() {
    private val homeDataLD = MutableLiveData<ArrayList<ViewData>>()
    val homeDataObserver get() = homeDataLD


    fun getHomeData(url: String) {
        apiHitter().getAllData(url)
            .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                homeDataLD.value = it
            }, {
                homeDataLD.value = null

            })
    }
}