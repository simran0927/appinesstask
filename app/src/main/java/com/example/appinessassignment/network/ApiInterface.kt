package com.example.appinessassignment.network

import com.example.appinessassignment.model.ViewData
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Url

interface ApiInterface {

    @GET
    fun getAllData(@Url url: String):Single<ArrayList<ViewData>>
}