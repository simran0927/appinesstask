package com.example.appinessassignment.model

import com.google.gson.annotations.SerializedName

data class ViewData(
    @SerializedName("amt.pledged")
    var amt_pledged: String,
    @SerializedName("blurb")
    var blurb: String,
    @SerializedName("by")
    var by: String,
    @SerializedName("country")
    var country: String,
    @SerializedName("currency")
    var currency: String,
    @SerializedName("end.time")
    var end_time: String,
    @SerializedName("location")
    var location: String,
    @SerializedName("num.backers")
    var num_backers: String,
    @SerializedName("percentage.funded")
    var percentage_funded: Int,
    @SerializedName("s.no")
    var s_no: Int,
    @SerializedName("state")
    var state: String,
    @SerializedName("title")
    var title: String,
    @SerializedName("type")
    var type: String,
    @SerializedName("url")
    var url: String
)