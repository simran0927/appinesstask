package com.simran.util.extension

import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.*
import android.content.ClipboardManager
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.BlendMode
import android.graphics.BlendModeColorFilter
import android.graphics.PorterDuff
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.text.*
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.UnderlineSpan
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.LinearInterpolator
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.annotation.ColorRes
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityOptionsCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.example.appinessassignment.R
import java.text.SimpleDateFormat
import java.util.*


///////////////////////////////////////// CONTEXT ////////////////////////////////////

/**
 * Util Function for startActivity
 *    open<BooksDetailActivity> {
 *      putExtra("IntentKey","DATA")
 *      putExtra("IntenKey@2", "DATA@2")
 *    }
 *  or
 * open<BooksDetailActivity>()
 * */
/*
inline fun <reified T> Activity.openActivity(extras: Intent.() -> Unit = {}) {
    val intent = Intent(this, T::class.java)
    intent.extras()
    val options = ActivityOptionsCompat.makeSceneTransitionAnimation(this)
    startActivity(intent, options.toBundle())
}
*/

inline fun <reified T> Fragment.openActivity(extras: Intent.() -> Unit = {}) {
    val intent = Intent(this.activity!!, T::class.java)
    intent.extras()
    val options = ActivityOptionsCompat.makeSceneTransitionAnimation(this.activity!!)
    startActivity(intent, options.toBundle())
}

inline fun <reified T> Activity.openActivityForResult(
    requestCode: Int,
    extras: Intent.() -> Unit = {}
) {
    val intent = Intent(this, T::class.java)
    intent.extras()
    val options = ActivityOptionsCompat.makeSceneTransitionAnimation(this)
    startActivityForResult(intent, requestCode, options.toBundle())
}

inline fun <reified T> Fragment.openActivityForResult(
    requestCode: Int,
    extras: Intent.() -> Unit = {}
) {
    val intent = Intent(this.activity!!, T::class.java)
    intent.extras()
    val options = ActivityOptionsCompat.makeSceneTransitionAnimation(this.activity!!)
    startActivityForResult(intent, requestCode, options.toBundle())
}


@Throws(IllegalAccessException::class, InstantiationException::class)
inline fun <reified T> newFragmentInstance(extras: Bundle.() -> Unit = {}): T? {

    return (T::class.java.newInstance() as Fragment).apply {
        arguments = Bundle().apply { extras() }
    } as T

}

@Throws(IllegalAccessException::class, InstantiationException::class)
inline fun <reified T> AppCompatActivity.showDialogFragment(extras: Bundle.() -> Unit = {}): T? {

    val instance = newFragmentInstance<T>(extras)
    (instance as DialogFragment).show(
        supportFragmentManager,
        T::class.java.simpleName
    )
    return instance
}

@Throws(IllegalAccessException::class, InstantiationException::class)
inline fun <reified T> Fragment.showDialogFragment(
    fromActivity: Boolean = true,
    extras: Bundle.() -> Unit = {}
): T? {
    val instance = newFragmentInstance<T>(extras)
    (instance as DialogFragment).show(
        if (fromActivity) {
            activity?.supportFragmentManager!!
        } else {
            childFragmentManager
        },
        T::class.java.simpleName
    )
    return instance
}



fun Bundle.putStrings(vararg values: Pair<String, String>) {
    values.forEach { value ->
        this.putString(value.first, value.second)
    }
}


fun Context.getColorCompat(@ColorRes color: Int): Int {
    return ContextCompat.getColor(this, color)
}

fun Activity.getColorCompat(@ColorRes color: Int): Int {
    return baseContext.getColorCompat(color)
}

fun Fragment.getColorCompat(@ColorRes color: Int): Int {
    return activity!!.getColorCompat(color)
}

fun Context.toast(msg: String) {
    val toast = Toast.makeText(this, msg, Toast.LENGTH_SHORT)
    val view = toast.view
    view.background.setColorFilter(ContextCompat.getColor(this, android.R.color.white))
    val text = view.findViewById<TextView>(android.R.id.message)
    text.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary))
    toast.show()
}


fun Drawable.setColorFilter(color: Int) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
        colorFilter = BlendModeColorFilter(color, BlendMode.SRC_ATOP)
    } else {
        setColorFilter(color, PorterDuff.Mode.SRC_ATOP)
    }
}

fun ImageView.setColorFilter(color: Int) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
        colorFilter = BlendModeColorFilter(color, BlendMode.SRC_ATOP)
    } else {
        setColorFilter(color, PorterDuff.Mode.SRC_ATOP)
    }
}


fun Fragment.toast(msg: String) {
    activity?.toast(msg)
}

fun Context.parseResColor(@ColorRes color: Int): Int {
    return ContextCompat.getColor(this, color)
}

fun Fragment.hideKeyboard() {
    activity?.hideKeyboard(view ?: View(activity))
}

fun Activity.hideKeyboard() {
    if (currentFocus == null) View(this) else currentFocus?.let { hideKeyboard(it) }
}

fun Context.hideKeyboard(view: View) {
    val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
}

fun Context.openKeyboard() {
    val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
    imm!!.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY)
}


fun Fragment.openKeyboard() {
    activity?.openKeyboard()
}

fun Activity.openKeyboard() {
    applicationContext?.openKeyboard()
}

fun Context.showAlert(
    message: String?,
    cancelable: Boolean = true,
    showPositiveButton: Boolean = true,
    work: () -> Unit = { }
) {

    if (message.isNullOrEmpty()) return

    val builder = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert)
    } else {
        AlertDialog.Builder(this)
    }

    builder.setMessage(message)
        .setCancelable(cancelable)

    if (showPositiveButton) {
        builder.setPositiveButton("Ok") { dialog, id ->
            work.invoke()
            dialog.dismiss()
        }
    }

    val alert = builder.create()
    alert.getButton(Dialog.BUTTON_NEGATIVE).isAllCaps = false
    alert.getButton(Dialog.BUTTON_POSITIVE).isAllCaps = false
    alert.show()
}

fun Context.showConfirmAlert(
    message: String?, positiveText: String?
    , negativeText: String?
    , onConfirmed: () -> Unit = {}
    , onCancel: () -> Unit = { }
) {

    if (message.isNullOrEmpty()) return

    val builder = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert)
    } else {
        AlertDialog.Builder(this)
    }


    builder.setMessage(message)
        .setCancelable(false)
        .setPositiveButton(positiveText) { dialog, _ ->
            onConfirmed.invoke()
            dialog.dismiss()
        }
        .setNegativeButton(negativeText) { dialog, _ ->
            onCancel.invoke()
            dialog.dismiss()
        }

    val alert = builder.create()
    alert.getButton(Dialog.BUTTON_NEGATIVE).isAllCaps = false
    alert.getButton(Dialog.BUTTON_POSITIVE).isAllCaps = false
    alert.show()
}


///////////////////////////////////////// VIEW ////////////////////////////////////

fun Activity.getDecorView(): View {
    return window.decorView
}

fun Fragment.getDecorView(): View {
    return activity?.window?.decorView!!
}

inline fun EditText.observeTextChange(crossinline body: (String) -> Unit) {
    addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(p0: Editable?) {}
        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            body(p0.toString())
        }
    })
}

inline fun TextView.observeTextChange(crossinline body: (String) -> Unit) {
    addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(p0: Editable?) {}
        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            body(p0.toString())
        }
    })
}

fun View.animateX(value: Float) {
    with(ObjectAnimator.ofFloat(this, View.TRANSLATION_X, value)) {
        duration = 3500
        repeatMode = ValueAnimator.REVERSE
        repeatCount = ValueAnimator.INFINITE
        interpolator = LinearInterpolator()
        start()
    }
}

fun View.animateY(value: Float) {
    with(ObjectAnimator.ofFloat(this, View.TRANSLATION_Y, value)) {
        duration = 3500
        repeatMode = ValueAnimator.REVERSE
        repeatCount = ValueAnimator.INFINITE
        interpolator = LinearInterpolator()
        start()
    }
}

infix fun ViewGroup.inflate(@LayoutRes view: Int): View {
    return LayoutInflater.from(context).inflate(view, this, false)
}

fun Int.inflate(viewGroup: ViewGroup): View {
    return LayoutInflater.from(viewGroup.context).inflate(this, viewGroup, false)
}

fun View.visible() {
    this.visibility = View.VISIBLE
}

fun View.gone() {
    this.visibility = View.GONE
}

fun View.invisible() {
    this.visibility = View.INVISIBLE
}

fun View.toggleVisibility() {
    when (this.visibility) {
        View.VISIBLE -> this.gone()
        View.INVISIBLE -> this.visible()
        View.GONE -> this.visible()
    }
}



///////////////////////////////////////// COMMON ////////////////////////////////////

inline fun <T> T.executeSafe(body: () -> Unit) {
    try {
        body.invoke()
    } catch (e: Exception) {

    }
}

fun <T> T.isNull(): Boolean {
    return this == null
}

fun <T> T.isNotNull(): Boolean {
    return this != null
}

inline infix operator fun Int.times(action: (Int) -> Unit) {
    var i = 0
    while (i < this) {
        action(i)
        i++
    }
}

fun String.remove(vararg value: String): String {
    var removeString = this
    value.forEach {
        removeString = removeString.replace(it, "")
    }
    return removeString
}

//*
//  android:textColorHighlight="#f00" // background color when pressed
//    android:textColorLink="#0f0" link backgroudnd color**/
fun TextView.makeLinks(vararg links: Pair<String, View.OnClickListener>) {
    val spannableString = SpannableString(this.text)
    for (link in links) {
        val clickableSpan = object : ClickableSpan() {
            override fun onClick(view: View) {
                Selection.setSelection((view as TextView).text as Spannable, 0)
                view.invalidate()
                link.second.onClick(view)
            }
        }
        val startIndexOfLink = this.text.toString().indexOf(link.first)
        spannableString.setSpan(
            clickableSpan, startIndexOfLink, startIndexOfLink + link.first.length,
            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
        )
    }
    this.movementMethod =
        LinkMovementMethod.getInstance() // without LinkMovementMethod, link can not click
    this.setText(spannableString, TextView.BufferType.SPANNABLE)
}

fun dpToPx(dp: Int): Int {
    return (dp * Resources.getSystem().displayMetrics.density).toInt()
}

fun pxToDp(px: Int): Int {
    return (px / Resources.getSystem().displayMetrics.density).toInt()
}

fun spToPx(sp: Int): Float {
    return TypedValue.applyDimension(
        TypedValue.COMPLEX_UNIT_SP,
        sp.toFloat(),
        Resources.getSystem().displayMetrics
    )
}

fun View.setTopMargin(top: Int) {
    if (layoutParams is ViewGroup.MarginLayoutParams) {
        val p = layoutParams as ViewGroup.MarginLayoutParams
        p.topMargin = dpToPx(top)
        requestLayout()
    }
    /*(view.layoutParams as RelativeLayout.LayoutParams).setMargins(
        position.left.convertDpToPx(context),
        position.top.convertDpToPx(context),
        position.right.convertDpToPx(context),
        position.bottom.convertDpToPx(context)
    )*/

}

fun View.setLeftMargin(left: Int) {
    if (layoutParams is ViewGroup.MarginLayoutParams) {
        val p = layoutParams as ViewGroup.MarginLayoutParams
        p.leftMargin = dpToPx(top)
        requestLayout()
    }
}

fun View.setRightMargin(right: Int) {
    if (layoutParams is ViewGroup.MarginLayoutParams) {
        val p = layoutParams as ViewGroup.MarginLayoutParams
        p.rightMargin = dpToPx(top)
        requestLayout()
    }
}

fun View.setBottomMargin(bottom: Int) {
    if (layoutParams is ViewGroup.MarginLayoutParams) {
        val p = layoutParams as ViewGroup.MarginLayoutParams
        p.bottomMargin = dpToPx(top)
        requestLayout()
    }
}

fun View.getMarginLayoutParams(): ViewGroup.MarginLayoutParams? {
    return if (layoutParams is ViewGroup.MarginLayoutParams) {
        layoutParams as ViewGroup.MarginLayoutParams
    } else {
        null
    }

}

fun String.underline(): SpannableString {
    val content = SpannableString(this)
    content.setSpan(UnderlineSpan(), 0, this.length, 0)
    return content
}

fun TextView.underline() {
    text = text.toString().underline()
}


fun String.copyToClipboard(context: Context) {
    val clipboard = context.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
    clipboard.setPrimaryClip(ClipData.newPlainText(this, this))
}

fun String.maintainTwoLength(): String {
    executeSafe {
        if (this.length < 2) {
            return "0$this"
        }
    }
    return this
}


inline fun <reified T : Activity> RecyclerView.ViewHolder.getActivity(): T? {
    val contextWrapperBaseContext = ((itemView.context as ContextWrapper).baseContext)
    val fieldOuterContext = contextWrapperBaseContext.javaClass.getDeclaredField("mOuterContext")
    fieldOuterContext.isAccessible = true
    val activity = fieldOuterContext.get(contextWrapperBaseContext) as? T
    fieldOuterContext.isAccessible = false
    return activity
}

fun Long.getDateInformat(): String? {
    val sd = SimpleDateFormat("EEE,dd MMM-hh:mm")
    val dateformatdate = sd.format(this)
    return dateformatdate
}

fun EditText.value(): String = this.text.toString()

fun TextView.value(): String = this.text.toString()




fun Spinner.onItemSelectListener(listener: (View?, Int) -> Unit) {
    this.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(parent: AdapterView<*>?) {}

        override fun onItemSelected(
            parent: AdapterView<*>?,
            view: View?,
            position: Int,
            id: Long
        ) {
            listener.invoke(view, position)
        }
    }
}