package com.simran.util.extension

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Handler
import androidx.core.app.ActivityOptionsCompat
import java.text.SimpleDateFormat
import java.util.*
import android.widget.Toast

import android.net.ConnectivityManager
import com.simran.app.App
import android.content.Intent.getIntent
import androidx.core.content.ContextCompat.startActivity
import android.content.DialogInterface




////////// delay ///////////////

inline fun Context.delay(timeMS: Long, crossinline body: () -> Unit): Handler {
    return Handler().apply {
        postDelayed({
            body()
        }, timeMS)
    }
}

inline fun <reified T> Activity.openActivity(extras: Intent.() -> Unit = {}) {
    val intent = Intent(this, T::class.java)
    intent.extras()
    val options = ActivityOptionsCompat.makeSceneTransitionAnimation(this)
    startActivity(intent, options.toBundle())
}


fun Long.toTimeFormat(dateFormat: String = "yyyy-MM-dd HH:mm"): String {

    val date = Date()
    date.time = this
    val toformat = SimpleDateFormat(dateFormat, Locale("en"))
    return toformat.format(date)
}

fun isNetworkAvailable(): Boolean {
    val conMgr =
        App.getApp().getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val netInfo = conMgr.activeNetworkInfo

    if (netInfo == null || !netInfo.isConnected || !netInfo.isAvailable) {
        Toast.makeText(App.getApp(), "No Internet connection!", Toast.LENGTH_LONG).show()
        return false
    }
    return true
}