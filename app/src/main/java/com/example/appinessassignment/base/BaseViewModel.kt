package com.example.appinessassignment.base

import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import org.koin.core.KoinComponent

open class BaseViewModel : ViewModel(), KoinComponent {
    val disposable by lazy { CompositeDisposable() }


    override fun onCleared() {
        disposable.clear()
        super.onCleared()
    }

}